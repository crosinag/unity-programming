﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthManager : MonoBehaviour
{

    public int MaxHealth;
    public int CurrentHealth;

    // Use this for initialization
    void Start()
    {
        CurrentHealth = MaxHealth;

    }

    // Update is called once per frame
    void Update()
    {
        if (CurrentHealth < 0)
        {
            Destroy (gameObject);


        }
    }
    //when collision happens damage is inflicted and subtracted from player/enemy health
    public void HurtEnemy(int damageToGive)
    {
        CurrentHealth -= damageToGive;
    }
    //PLAYER/ ENEMY has a certain amount of health
    public void SetMaxHealth()
    {
        CurrentHealth = MaxHealth;
    }
}