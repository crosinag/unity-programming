﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthManager : MonoBehaviour {

    public int playerMaxHealth;
    public int playerCurrentHealth;

	// Use this for initialization
	void Start () {
        playerCurrentHealth = playerMaxHealth;

	}
	
	// Update is called once per frame
	void Update () {
		if(playerCurrentHealth < 0)
        {
            gameObject.SetActive(false);


        }
	}
    //when collision happens damage is inflicted and subtracted from player/enemy health
    public void HurtPlayer(int damageToGive)
    {
        playerCurrentHealth -= damageToGive;
    }
    //PLAYER/ ENEMY has a certain amount of health
    public void SetMaxHealth()
    {
        playerCurrentHealth = playerMaxHealth;
    }
}
